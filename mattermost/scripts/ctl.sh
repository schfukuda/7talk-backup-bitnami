#!/bin/sh

MATTERMOST_START="/opt/bitnami/apps/mattermost/server/bin/mattermost --config /opt/bitnami/apps/mattermost/server/config/config.json --disableconfigwatch server"
MATTERMOST_STATUS=""
PID=""
ERROR=0

get_mattermost_pid() {
    PID=`ps aux | grep "mattermost --config" | grep server | grep -v grep | awk '{print $2}'`
    if [ ! "$PID" ]; then
        return
    fi
}

is_service_running() {
    PID=$1
    if [ "x$PID" != "x" ] && kill -0 $PID 2>/dev/null ; then
        RUNNING=1
    else
        RUNNING=0
    fi
    return $RUNNING
}

is_mattermost_running() {
    get_mattermost_pid
    is_service_running $PID
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        MATTERMOST_STATUS="mattermost not running"
    else
        MATTERMOST_STATUS="mattermost already running"
    fi
    return $RUNNING
}

start_mattermost() {
    is_mattermost_running
    RUNNING=$?
    if [ $RUNNING -eq 1 ]; then
        echo "$0 $ARG: mattermost (pid $PID) already running"
    else
        cd /opt/bitnami/apps/mattermost/server
        if [ `id|sed -e s/uid=//g -e s/\(.*//g` -eq 0 ]; then
            su mattermost -s /bin/sh -c "$MATTERMOST_START > /dev/null 2>&1 &"
        else
            $MATTERMOST_START > /dev/null 2>&1 &
        fi
        sleep 1
        is_mattermost_running
        RUNNING=$?
      if [ $RUNNING -eq 1 ];  then
            echo "$0 $ARG: mattermost started"
      else
            echo "$0 $ARG: mattermost could not be started"
            ERROR=1
      fi
    fi
}

stop_mattermost() {
    is_mattermost_running
    RUNNING=$?
    if [ $RUNNING -eq 0 ]; then
        echo "$0 $ARG: $MATTERMOST_STATUS"
        exit
    fi
    get_mattermost_pid
    kill -9 $PID
    sleep 1
    is_mattermost_running
    RUNNING=$?
    COUNTER=10

    while [ $RUNNING -ne 0 ] && [ $COUNTER -ne 0 ]; do
        COUNTER=`expr $COUNTER - 1`
        sleep .5
        is_mattermost_running
        RUNNING=$?
    done

    if [ $RUNNING -eq 0 ]; then
        echo "$0 $ARG: mattermost stopped"
    else
        echo "$0 $ARG: mattermost could not be stopped"
        ERROR=2
    fi
}

if [ "x$1" = "xstart" ]; then
    start_mattermost
elif [ "x$1" = "xstop" ]; then
    stop_mattermost
elif [ "x$1" = "xstatus" ]; then
    is_mattermost_running
    echo $MATTERMOST_STATUS
fi

exit $ERROR
